import React, { useEffect, useState } from 'react';
import { Text, View, FlatList, StyleSheet } from 'react-native';
import { Card } from 'react-native-elements';
import Toast from 'react-native-toast-message';
import { useAsyncStorage } from '@react-native-async-storage/async-storage'
import { TouchableOpacity } from 'react-native-gesture-handler';

export default function HomeScreen({ navigation }) {
    const { setItem, getItem } = useAsyncStorage('todo');
    const [ items, setItems ] = useState([]);
    const [ loading, setLoading ] = useState(true);

    function getTodoList () {
        getItem().then((todoJSON) => {
            const todo = todoJSON ? JSON.parse(todoJSON).sort((a, b) => (a.done === b.done) ? 0 : a.done ? 1 : -1) : [];

            setItems(todo);
            setLoading(false);
        })
        .catch((err) => {
            console.error(err);
            Toast.show({
                type: 'error',
                text1: 'An error occurred',
                position: 'top'
            });
        });
    }

    function markAsDone(item) {
        if (!item)
            return;

        item.done = !item.done;

        setItem(JSON.stringify(items)).then(() => {
            getTodoList();
        }).catch((err) => {
            console.error(err);

            Toast.show({
                type: 'error',
                text1: 'An error occurred and a new iotem could not be saved',
                position: 'top'
            });
        });
    }

    function renderCard ({ item }) {
        return (
            <TouchableOpacity onPress={() => markAsDone(item)}>
                <Card>
                    <Card.Title style={(item.done) ? styles.cardTitleDone : styles.cardTitle}>{ item.title }</Card.Title>
                    <View>
                        <Text>{(item.done) ? 'DONE' : 'UNDONE'}</Text>
                    </View>
                </Card>
            </TouchableOpacity>
        );
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', getTodoList);

        return unsubscribe;
    });

    return (
        <View>
            <FlatList refreshing={loading} onRefresh={getTodoList} style={styles.list} data={items}
                renderItem={renderCard} keyExtractor={(item) => item.id} />
        </View>
    );
}

const styles = StyleSheet.create({
    list: {
        width: '100%'
    },
    cardTitle: {
        textAlign: 'left'
    },
    cardTitleDone: {
        textAlign: 'left',
        textDecorationLine: 'line-through',
        textDecorationStyle: 'double',
        textDecorationColor: '#FF0000',
        color: '#AAA'
    }
});