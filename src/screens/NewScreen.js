import React from 'react';
import { Formik } from 'formik';
import { StyleSheet, View, Button } from 'react-native';
import { Text, Input } from 'react-native-elements';
import Toast from 'react-native-toast-message';
import { useAsyncStorage } from '@react-native-async-storage/async-storage';
import uuid from 'react-native-uuid';


export default function NewScreen ({ navigation }) {
    const { getItem, setItem } = useAsyncStorage('todo');

    function newTask (values) {
        if (!values.title) {
            Toast.show({
                type: 'error',
                text1: 'Title is required',
                position: 'top'
            });

            return;
        }

        getItem().then((todoJSON) => {
            let todo = todoJSON ? JSON.parse(todoJSON) : [];

            todo.push({
                id: uuid.v4(),
                title: values.title,
                done: false
            });

            setItem(JSON.stringify(todo)).then(() => {
                navigation.goBack();
            }).catch((err) => {
                console.error(err);

                Toast.show({
                    type: 'error',
                    text1: 'An error occurred and a new iotem could not be saved',
                    position: 'top'
                });
            });
        }).catch((err) => {
            console.error(err);

            Toast.show({
                type: 'error',
                text1: 'An error occurred and a new iotem could not be saved',
                position: 'bottom'
            });
        })
    }

    return (
        <Formik
            initialValues={{ title: '' }}
            onSubmit={ newTask }
        >
            {({ handleChange, handleBlur, handleSubmit, values }) => (
                <View style={style.container}>
                    <Text h4>New Todo item</Text>
                    <Input
                        placeholder="Example: Cook, clean, etc..."
                        onChangeText={handleChange('title')}
                        onBlur={handleBlur('title')}
                        style={style.input}
                    />
                    <Button title="Add" onPress={handleSubmit} style={style.button} />
                </View>
            )}
        </Formik>
    )
};

const style = StyleSheet.create({
    container: {
        marginTop: 10,
        padding: 10
    },
    input: {
        marginTop: 10
    },
    button: {
      backgroundColor: '#228CDB'
    }
});